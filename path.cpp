#include "mainwindow.h"
#include "path.h"

Path::Path() {
    type = 3;
    time = 0;
    lastTurningPointIndex = 0;
    lines.append(new Line());
    simulated = false;
}

Path::Path(Pos* initPos) {
    type = 3;
    time = 0;
    lastTurningPointIndex = 0;
    lines.append(new Line(initPos));
    addPos(initPos);
    turningPoints.append(initPos);
    simulated = false;
}

QRectF Path::boundingRect() const {
    return QRectF(0, 0, 700, 650);
}

void Path::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    QRectF rect = boundingRect();

}

Pos* Path::getPos(int index) {
    if(index < coordHistory.size() && index >= 0) {
        return coordHistory.at(index);
    }
}

Pos* Path::getCurrentPos() {
    if(coordHistory.isEmpty()) {
        return new Pos();
    }

    int currTime = 0;
    if(!simulated) {
        currTime = MainWindow::currentTime;
    } else {
        currTime = MainWindow::currentTimeSub;
    }

    for(int i = coordHistory.size() - 1; i >= 0; i--) {
        if(coordHistory.at(i)->time <= currTime) {
            return coordHistory.at(i);
        }
    }
    return coordHistory.at(0);
}

Pos* Path::getLastPos() {
    if(coordHistory.size() > 0) {
        return coordHistory.last();
    }
}

void Path::addPos(Pos* newPos) {
    if(!simulated) {
        newPos->time = MainWindow::timePassed;
    } else {
        newPos->time = MainWindow::timePassedSub;
    }

    newPos->rmsX = getRootMeanSquareX();
    newPos->rmsY = getRootMeanSquareY();
    newPos->rmsC = getRootMeanSquareCourse();
    newPos->rmsV = getRootMeanSquareV();
    coordHistory.append(newPos);

    if(lines.last()->getBeginning()->course != newPos->course) {
        lines.last()->addPos(newPos);
        lines.append(new Line(newPos));
    }
    lines.last()->setSimulated(simulated);
    lines.last()->addPos(newPos);
    lines.last()->setParentItem(this);
}

double Path::getRootMeanSquareX() {
    if(coordHistory.isEmpty()) {
        return 0;
    }
    double rms = 0, sum = 0, out = 0;
    if(coordHistory.size() > 0) {
        for(int i = 0; i < coordHistory.size(); i++) {
            rms += coordHistory.at(i)->x;
        }
        rms = rms / coordHistory.size();

        for(int i = 0; i < coordHistory.size(); i++) {
            sum += pow(coordHistory.at(i)->x - rms, 2);
        }

        out = sqrt(sum / coordHistory.size());
    }
    return out;
}

double Path::getRootMeanSquareY() {
    if(coordHistory.isEmpty()) {
        return 0;
    }
    double rms = 0, sum = 0, out = 0;
    if(coordHistory.size() > 0) {
        for(int i = 0; i < coordHistory.size(); i++) {
            rms += coordHistory.at(i)->y;
        }
        rms = rms / coordHistory.size();

        for(int i = 0; i < coordHistory.size(); i++) {
            sum += pow(coordHistory.at(i)->y - rms, 2);
        }

        out = sqrt(sum / coordHistory.size());
    }
    return out;
}

double Path::getMeanCourse() {
    if(coordHistory.isEmpty()) {
        return 0;
    }
    double mean = 0;
    if(coordHistory.size() > 0) {
        for(int i = 0; i < coordHistory.size(); i++) {
            mean += coordHistory.at(i)->course;
        }
        mean = mean / coordHistory.size();
    }
    return mean;
}

double Path::getRootMeanSquareCourse() {
    if(coordHistory.isEmpty()) {
        return 0;
    }
    double rms = 0, sum = 0, out = 0;
    if(coordHistory.size() > 0) {
        for(int i = 0; i < coordHistory.size(); i++) {
            rms += coordHistory.at(i)->course;
        }
        rms = rms / coordHistory.size();

        for(int i = 0; i < coordHistory.size(); i++) {
            sum += pow(coordHistory.at(i)->course - rms, 2);
        }

        out = sqrt(sum / coordHistory.size());
    }
    return out;
}

double Path::getRootMeanSquareV() {
    if(coordHistory.isEmpty()) {
        return 0;
    }
    double rms = 0, sum = 0, out = 0;
    if(coordHistory.size() > 0) {
        for(int i = 0; i < coordHistory.size(); i++) {
            rms += coordHistory.at(i)->v;
        }
        rms = rms / coordHistory.size();

        for(int i = 0; i < coordHistory.size(); i++) {
            sum += pow(coordHistory.at(i)->v - rms, 2);
        }

        out = sqrt(sum / coordHistory.size());
    }
    return out;
}

void Path::setChoosen(bool choosen) {
    this->choosen = choosen;

    double width = 1;
    if(choosen) {
        width = 2;
    }
    for(int i = 0; i < lines.size(); i++) {
        lines.at(i)->pen.setWidth(width);
        lines.at(i)->setAcceptHoverEvents(choosen);
    }
}

#include "line.h"
#include "mainwindow.h"

Line::Line() {
    coordinates.append(new Pos());
}

Line::Line(Pos* beginning) {
    coordinates.append(beginning);
    dx = 0.0;
    dy = 0.0;
    forceLast = -1;
    pen.setColor(Qt::black);
    pen.setWidth(1);
}

Line::Line(Pos* beginning, Pos* end) {
    coordinates.append(beginning);
    coordinates.append(end);
    dx = end->x - beginning->x;
    dy = end->y - beginning->y;
    forceLast = -1;
    pen.setColor(Qt::black);
    pen.setWidth(1);
}

void Line::update() {
    dx = getEnd()->x - getBeginning()->x;
    dy = getEnd()->y - getBeginning()->y;
    if(acceptHoverEvents()) {
        setToolTip(QString::number(sqrt(dx * dx + dy * dy)));
    }
    QGraphicsItem::update();
}

void Line::addPos(Pos* newPos) {
    coordinates.append(newPos);
    update();
}

QRectF Line::boundingRect() const {
    double x = getBeginning()->x, y = -getBeginning()->y, w = getBeginning()->x, h = -getBeginning()->y;
    for(int i = 1; i < coordinates.size(); i++) {
        if(coordinates.at(i)->x < x) {
            x = coordinates.at(i)->x;
        }
        if(-coordinates.at(i)->y < y) {
            y = -coordinates.at(i)->y;
        }

        if(coordinates.at(i)->x > w) {
            w = coordinates.at(i)->x;
        }
        if(-coordinates.at(i)->y > h) {
            h = -coordinates.at(i)->y;
        }
    }

    w = w - x;
    h = h - y;

    return QRectF(x - 2 + 12.0, y - 2 + 12.0, w + 4, h + 4);
}

void Line::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    QRectF rect = boundingRect();
    painter->setPen(pen);
    int currTime = 0, timePassed = 0;
    if(!simulated) {
        currTime = MainWindow::currentTime;
        timePassed = MainWindow::timePassed;
    } else {
        currTime = MainWindow::currentTimeSub;
        timePassed = MainWindow::timePassedSub;
    }
    if(coordinates.size() > 1) {
        if(currTime > getBeginning()->time) {
            /*if(getEnd()->time > currTime) {
                for(int i = coordinates.size() - 1; i >= 0; i--) {
                    if(coordinates.at(i)->time <= currTime) {
                        forceLast = i;
                    }
                }
            } else {
                forceLast = -1;
            }*/
            update();
            painter->drawLine(getBeginning()->x + 12.0, -getBeginning()->y + 12.0, getEnd()->x + 12.0, -getEnd()->y + 12.0);
        }
    }
}

void Line::hoverEnterEvent(QGraphicsSceneHoverEvent *event) {
    pen.setColor(Qt::blue);
}

void Line::hoverLeaveEvent(QGraphicsSceneHoverEvent *event) {
    pen.setColor(Qt::black);
}

Pos* Line::get(int i) const {
    if(i < coordinates.size()) {
        return coordinates.at(i);
    } else {
        return coordinates.last();
    }
}

Pos* Line::getBeginning() const {
    return coordinates.at(0);
}

Pos* Line::getEnd() const {
    if(forceLast != -1 && forceLast < coordinates.size()) {
        return coordinates.at(forceLast);
    }

    return coordinates.last();
}

int Line::getSize() {
    return coordinates.size();
}

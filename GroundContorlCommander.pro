#-------------------------------------------------
#
# Project created by QtCreator 2015-05-14T10:22:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GroundContorlCommander
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    obj.cpp \
    pos.cpp \
    path.cpp \
    layer.cpp \
    newobjectdialog.cpp \
    line.cpp \
    breach.cpp \
    scouting.cpp \
    searching.cpp

HEADERS  += mainwindow.h \
    obj.h \
    pos.h \
    path.h \
    layer.h \
    newobjectdialog.h \
    line.h \
    breach.h \
    scouting.h \
    searching.h

FORMS    += mainwindow.ui\
    newobjectdialog.ui

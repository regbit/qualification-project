#include "scouting.h"

Scouting::Scouting()
{
    type = 1;
    time = 0;
    drawBounds = false;
    lastTurningPointIndex = 0;
    lines.append(new Line());
    simulated = false;
}

Scouting::Scouting(Pos* initPos, double genCourse, double width, double length)
{
    type = 1;
    time = 0;
    drawBounds = false;
    this->genCourse = genCourse;
    this->width = width;
    this->length = length;
    lastTurningPointIndex = 0;
    lines.append(new Line(initPos));
    addPos(initPos);
    turningPoints.append(initPos);
    simulated = false;
}

Scouting::Scouting(Path* path)
{
    type = 1;
    drawBounds = false;
    time = path->getTime();
    choosen = path->isChoosen();
    lastTurningPointIndex = path->getLastTurningPointIndex();
    lines = path->getLines();
    coordHistory = path->getCoordHistory();
    turningPoints = path->getTurningPoints();
    simulated = path->isSimulated();
}

QRectF Scouting::boundingRect() const
{
    return QRectF(0, 0, 700, 650);
}

void Scouting::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QRectF rect = boundingRect();
    if (!choosen) {
        return;
    }
    if (drawBounds) {
        double x1 = 0, x2 = 0;
        double y1 = 0, y2 = 0;
        double transformedX = 0, transformedY = 0, angle = PI * (450 - genCourse) / 180;
        transformedX = coordHistory.at(0)->x;
        transformedY = coordHistory.at(0)->y;
        transformedX = transformedX * cos(angle) - transformedY * sin(angle);
        transformedY = transformedX * sin(angle) + transformedY * cos(angle);
        double width = this->width;

        transformedX = -width / 2;
        transformedY = 0;
        x1 = transformedX * cos(angle) + transformedY * sin(angle);
        y1 = -transformedX * sin(angle) + transformedY * cos(angle);
        transformedY = -700;
        x2 = transformedX * cos(angle) + transformedY * sin(angle);
        y2 = -transformedX * sin(angle) + transformedY * cos(angle);
        pen.setColor(Qt::blue);
        painter->setPen(pen);
        painter->drawLine(x1 + coordHistory.at(0)->x, -(y1 + coordHistory.at(0)->y), x2 + coordHistory.at(0)->x, -(y2 + coordHistory.at(0)->y));

        transformedX = 0;
        transformedY = 0;
        x1 = transformedX * cos(angle) + transformedY * sin(angle);
        y1 = -transformedX * sin(angle) + transformedY * cos(angle);
        transformedY = -700;
        x2 = transformedX * cos(angle) + transformedY * sin(angle);
        y2 = -transformedX * sin(angle) + transformedY * cos(angle);
        pen.setColor(Qt::gray);
        painter->setPen(pen);
        painter->drawLine(x1 + coordHistory.at(0)->x, -(y1 + coordHistory.at(0)->y), x2 + coordHistory.at(0)->x, -(y2 + coordHistory.at(0)->y));

        transformedX = -width;
        transformedY = 0;
        x1 = transformedX * cos(angle) + transformedY * sin(angle);
        y1 = -transformedX * sin(angle) + transformedY * cos(angle);
        transformedY = -700;
        x2 = transformedX * cos(angle) + transformedY * sin(angle);
        y2 = -transformedX * sin(angle) + transformedY * cos(angle);
        painter->drawLine(x1 + coordHistory.at(0)->x, -(y1 + coordHistory.at(0)->y), x2 + coordHistory.at(0)->x, -(y2 + coordHistory.at(0)->y));
    }
}

void Scouting::update() {
    if (simulated) {
        double course = getLastPos()->course;
        double angle = PI * (450 - course) / 180;
        double x = getLastPos()->x + getLastPos()->v * cos(angle);
        double y = getLastPos()->y + getLastPos()->v * sin(angle);
        double delta = sqrt(pow(getPos(lastTurningPointIndex)->x - x, 2) + pow(getPos(lastTurningPointIndex)->y - y, 2));
        angle = PI * (450 - genCourse) / 180;
        if (course != genCourse) {
            if (delta >= width) {
                double b = delta - width;
                double a = getLastPos()->v - b;
                x = getLastPos()->x + a * cos(angle);
                y = getLastPos()->y + a * sin(angle);
                course = genCourse;
                addPos(new Pos(x, y, course, getLastPos()->v, true));
                turningPoints.append(getLastPos());
                angle = PI * (450 - course) / 180;
                x = getLastPos()->x + b * cos(angle);
                y = getLastPos()->y + b * sin(angle);
                lastTurningPointIndex = getSize() - 1;
            }
        } else {
            if (delta >= length) {
                double b = delta - length;
                double a = getLastPos()->v - b;
                x = getLastPos()->x + a * cos(angle);
                y = getLastPos()->y + a * sin(angle);
                if (genCourse < getPos(lastTurningPointIndex - 1)->course) {
                    course -= 90;
                } else if (genCourse > getPos(lastTurningPointIndex - 1)->course) {
                    course += 90;
                }
                addPos(new Pos(x, y, course, getLastPos()->v, true));
                turningPoints.append(getLastPos());
                angle = PI * (450 - course) / 180;
                x = getLastPos()->x + b * cos(angle);
                y = getLastPos()->y + b * sin(angle);
            }
        }
        addPos(new Pos(x, y, course, getLastPos()->v, true));
    }
}

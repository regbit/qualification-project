#ifndef POS_H
#define POS_H

class Pos {

public:
    double x, y, course, v;
    double rmsX, rmsY, rmsC, rmsV;
    bool simulated;
    int time;

    Pos(bool isSimulated = false);
    Pos(double x, double y, double course, double v, bool isSimulated = false);
};

#endif // POS_H

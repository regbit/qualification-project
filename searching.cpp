#include "searching.h"

Searching::Searching() {
    type = 2;
    time = 0;
    drawBounds = false;
    lastTurningPointIndex = 0;
    lines.append(new Line());
    simulated = false;
}

Searching::Searching(Pos* initPos, double radius) {
    type = 2;
    time = 0;
    drawBounds = false;
    this->radius = radius;
    lastTurningPointIndex = 0;
    lines.append(new Line(initPos));
    addPos(initPos);
    turningPoints.append(initPos);
    simulated = false;
}

Searching::Searching(Path* path) {
    type = 2;
    drawBounds = false;
    time = path->getTime();
    choosen = path->isChoosen();
    lastTurningPointIndex = path->getLastTurningPointIndex();
    lines = path->getLines();
    coordHistory = path->getCoordHistory();
    turningPoints = path->getTurningPoints();
    simulated = path->isSimulated();
}

QRectF Searching::boundingRect() const {
    return QRectF(0, 0, 700, 650);
}

void Searching::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    QRectF rect = boundingRect();
    pen.setColor(Qt::gray);
    painter->setPen(pen);
    painter->drawEllipse(getPos(0)->x, getPos(0)->y, radius * 2, radius * 2);
    if(!choosen) {return;}
}

void Searching::update() {
    if(simulated) {
        double course = getLastPos()->course;
        double angle = PI * (450 - course)/180;
        double x = getLastPos()->x + getLastPos()->v * cos(angle);
        double y = getLastPos()->y + getLastPos()->v * sin(angle);
        double delta = sqrt(pow(x - getPos(lastTurningPointIndex)->x, 2)  + pow(y - getPos(lastTurningPointIndex)->y, 2));
        if(abs(delta) > radius - rand()%((int)radius/10) + ((int)radius/20)) {
            double b = abs(abs(delta) - radius);
            double a = getLastPos()->v - b;
            x = getLastPos()->x + a * cos(angle);
            y = getLastPos()->y + a * sin(angle);
            course = ((int)course + 180)%360 + rand() % 180 - 90;
            addPos(new Pos(x, y, course, getLastPos()->v, true));
            turningPoints.append(getLastPos());
            angle = PI * (450 - course)/180;
            x = getLastPos()->x + b * cos(angle);
            y = getLastPos()->y + b * sin(angle);
        }
        addPos(new Pos(x, y, course, getLastPos()->v, true));
    }
}

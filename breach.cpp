#include "breach.h"

/// Описание класса задачи "Прорыв"
Breach::Breach() {
    type = 0;
    time = 0;
    turned = false;
    drawBounds = false;
    lastTurningPointIndex = 0;
    lines.append(new Line());
    simulated = false;
}

Breach::Breach(Pos* initPos, double genCourse, double deltaCourse, double dist) {
    type = 0;
    time = 0;
    turned = false;
    drawBounds = false;
    this->genCourse = genCourse;
    this->deltaCourse = deltaCourse;
    this->dist = dist;
    lastTurningPointIndex = 0;
    lines.append(new Line(initPos));
    addPos(initPos);
    turningPoints.append(initPos);
    simulated = false;
}

Breach::Breach(Path* path) {
    type = 0;
    turned = false;
    drawBounds = false;
    time = path->getTime();
    choosen = path->isChoosen();
    lastTurningPointIndex = path->getLastTurningPointIndex();
    lines = path->getLines();
    coordHistory = path->getCoordHistory();
    turningPoints = path->getTurningPoints();
    simulated = path->isSimulated();
}

QRectF Breach::boundingRect() const {
    return QRectF(0, 0, 700, 650);
}

void Breach::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    QRectF rect = boundingRect();
    if(!choosen) {return;}
    if(drawBounds) {
        double x1 = 0, x2 = 0;
        double y1 = 0, y2 = 0;
        double transformedX = 0, transformedY = 0, angle = PI * (450 - genCourse)/180;
        transformedX = coordHistory.at(0)->x;
        transformedY = coordHistory.at(0)->y;
        transformedX = transformedX * cos(angle) - transformedY * sin(angle);
        transformedY = transformedX * sin(angle) + transformedY * cos(angle);
        double width = dist;

        transformedX = 0;
        transformedY = 0;
        x1 = transformedX * cos(angle) + transformedY * sin(angle);
        y1 = -transformedX * sin(angle) + transformedY * cos(angle);
        transformedY = -700;
        x2 = transformedX * cos(angle) + transformedY * sin(angle);
        y2 = -transformedX * sin(angle) + transformedY * cos(angle);
        pen.setColor(Qt::blue);
        painter->setPen(pen);
        painter->drawLine(x1 + coordHistory.at(0)->x, -(y1 + coordHistory.at(0)->y), x2 + coordHistory.at(0)->x, -(y2 + coordHistory.at(0)->y));

        transformedX = width/2;
        transformedY = 0;
        x1 = transformedX * cos(angle) + transformedY * sin(angle);
        y1 = -transformedX * sin(angle) + transformedY * cos(angle);
        transformedY = -700;
        x2 = transformedX * cos(angle) + transformedY * sin(angle);
        y2 = -transformedX * sin(angle) + transformedY * cos(angle);
        pen.setColor(Qt::gray);
        painter->setPen(pen);
        painter->drawLine(x1 + coordHistory.at(0)->x, -(y1 + coordHistory.at(0)->y), x2 + coordHistory.at(0)->x, -(y2 + coordHistory.at(0)->y));

        transformedX = -width/2;
        transformedY = 0;
        x1 = transformedX * cos(angle) + transformedY * sin(angle);
        y1 = -transformedX * sin(angle) + transformedY * cos(angle);
        transformedY = -700;
        x2 = transformedX * cos(angle) + transformedY * sin(angle);
        y2 = -transformedX * sin(angle) + transformedY * cos(angle);
        painter->drawLine(x1 + coordHistory.at(0)->x, -(y1 + coordHistory.at(0)->y), x2 + coordHistory.at(0)->x, -(y2 + coordHistory.at(0)->y));
    }
}

void Breach::update() {
    QGraphicsItem::update(0,0,700,650);
    if(simulated) {
        double course = getLastPos()->course;
        double angle = PI * (450 - course)/180;
        double x = getLastPos()->x + getLastPos()->v * cos(angle);
        double y = getLastPos()->y + getLastPos()->v * sin(angle);
        double transformedX = 0, transformedY = 0;
        angle = PI * (450 - genCourse)/180;
        double displaycedX = x - getPos(0)->x;
        double displaycedY = y - getPos(0)->y;
        transformedX = displaycedX * cos(angle) - displaycedY * sin(angle);
        transformedY = displaycedX * sin(angle) + displaycedY * cos(angle);

        if(abs(transformedX) >= dist/2) {
            double b = abs(abs(transformedX) - dist/2);
            double a = getLastPos()->v - b;
            x = getLastPos()->x + a * cos(angle);
            y = getLastPos()->y + a * sin(angle);

            if(!turned) {
                course -= deltaCourse + rand() % 100 / 100 * deltaCourse / 5 - deltaCourse / 10;
            } else {
                course += deltaCourse + rand() % 100 / 100 * deltaCourse / 5 - deltaCourse / 10;
            }
            addPos(new Pos(x, y, course, getLastPos()->v, true));
            turningPoints.append(getLastPos());

            angle = PI * (450 - course)/180;
            x = getLastPos()->x + b * cos(angle);
            y = getLastPos()->y + b * sin(angle);
            turned = !turned;
        }
        addPos(new Pos(x, y, course, getLastPos()->v, true));
    }
}

#ifndef LAYER_H
#define LAYER_H

#include <QtCore>
#include <QtGui>

class Layer : public QGraphicsItem {
public:
    Layer();
    virtual ~Layer(){}

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // LAYER_H

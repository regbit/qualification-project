#ifndef BREACH_H
#define BREACH_H
#include "path.h"

class Breach : public Path {

public:
    Breach();
    Breach(Pos* initPos, double genCourse, double deltaCourse, double dist);
    Breach(Path* path);
    QString getName(){return "������";}

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void update();
private:
    double genCourse, deltaCourse, dist;
    bool turned;
};

#endif // BREACH_H

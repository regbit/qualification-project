#ifndef OBJ_H
#define OBJ_H

#include <QtCore>
#include <QtGui>
#include "path.h"

class Obj : public QGraphicsItem {

public:
    Obj(Path *path, QString name, int timeCreated);

    static const int LOW_PRIORITY = 4, MEDIUM_PRIORITY = 5, HIGH_PRIORITY = 6;

    //Getters
    Path* getPath() {return path;}
    Path* getPredictedPath() {return predictedPath;}
    Path* getSpottedPath() {return spottedPath;}
    double getX() {return spottedPath->getCurrentPos()->x;}
    double getY() {return -spottedPath->getCurrentPos()->y;}
    double getCourse() {return spottedPath->getCurrentPos()->course;}
    double getV() {return spottedPath->getCurrentPos()->v;}
    double getRootMeanSquareX() {return spottedPath->getCurrentPos()->rmsX;}
    double getRootMeanSquareY() {return spottedPath->getCurrentPos()->rmsY;}
    double getRootMeanSquareCourse() {return spottedPath->getCurrentPos()->rmsC;}
    double getRootMeanSquareV() {return spottedPath->getCurrentPos()->rmsV;}
    int getPriority() {return priority;}
    int getTimeCreated() {return timeCreated;}
    QString getPriorityName();
    QString getName() {return name;}
    bool isClicked() {return clicked;}
    bool isChosen() {return chosen;}
    bool isPredicted() {return predicted;}
    bool isShowRealPath() {return showRealPath;}

    //Setters
    void setPriority(int priority) {this->priority = priority;}
    void setName(QString name) {this->name = name;}
    void setClicked(bool clicked) {this->clicked = clicked;}
    void setChosen(bool chosen) {this->chosen = chosen;}
    void setPredicted(bool predicted) {this->predicted = predicted;}
    void setShowRealPath(bool showRealPath) {this->showRealPath = showRealPath;}
    void setPath(Path *p) {this->path = p;}
    void setTimeCreated(int timeCreated) {this->timeCreated = timeCreated;}
    void setSimulated(bool isSimulated) {simulated = isSimulated; path->setSimulated(isSimulated); predictedPath->setSimulated(isSimulated); spottedPath->setSimulated(isSimulated);}
    void setSpottedChance(double chance){spottedChance = chance;}

    //Destructor
    ~Obj() {
        delete path;
        delete predictedPath;
        delete spottedPath;
    }

    //Grapshics
    void updatePos(int time);

    QPainterPath shape() const;
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    QComboBox *getPriorityBox() const;
    void setPriorityBox(QComboBox *value);

    QTableWidgetItem *getNameLabel() const;
    void setNameLabel(QTableWidgetItem *value);

    QTableWidgetItem *getTypeLabel() const;
    void setTypeLabel(QTableWidgetItem *value);

    //Other
    bool spotted(double chance);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

    void analyze();
    bool predict();

private:
    Path *path, *predictedPath, *spottedPath;
    int suggestedType, priority, timeCreated;
    bool chosen, clicked, predicted, analyzed, simulated, showRealPath;
    double spottedChance;
    QString name;

    QComboBox *priorityBox;
    QTableWidgetItem *nameLabel, *typeLabel;
};

#endif // OBJ_H

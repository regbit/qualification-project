#include "obj.h"
#include "mainwindow.h"
#include "breach.h"
#include "scouting.h"
#include "searching.h"

Obj::Obj(Path *path, QString name, int timeCreated) {
    setName(name);
    setPriority(Obj::HIGH_PRIORITY);
    chosen = false;
    clicked = false;
    predicted = false;
    analyzed = false;
    simulated = false;
    showRealPath = false;

    this->timeCreated = timeCreated;

    this->path = path;
    this->path->pen.setColor(Qt::black);
    this->path->pen.setWidth(1);
    this->path->setSimulated(false);

    predictedPath = new Path();
    predictedPath->pen.setColor(Qt::gray);
    predictedPath->pen.setWidth(1);
    predictedPath->pen.setStyle(Qt::DashLine);

    spottedPath = new Path();
    predictedPath->pen.setColor(Qt::red);
    predictedPath->pen.setWidth(1);
    suggestedType = 0;

    spottedChance = 0.2;
}

QPainterPath Obj::shape() const {
    QPainterPath path;
    path.addEllipse(boundingRect());
    return path;
}

QRectF Obj::boundingRect() const {
    return QRectF(0, 0, 24, 24);
}

void Obj::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    QRectF rect = boundingRect();
    for(int i = 0; i < predictedPath->getLines().size(); i++) {
        predictedPath->getLines().at(i)->pen.setColor(Qt::gray);
        predictedPath->getLines().at(i)->pen.setWidth(1);
        predictedPath->getLines().at(i)->pen.setStyle(Qt::DashLine);
    }

    for(int i = 0; i < spottedPath->getLines().size(); i++) {
        spottedPath->getLines().at(i)->pen.setColor(Qt::red);
        spottedPath->getLines().at(i)->pen.setWidth(1);
    }


    if(chosen) {
        //Object
        QPen pen2(Qt::blue, 2);
        painter->setPen(pen2);

        path->setChoosen(true);
        predictedPath->setChoosen(true);

        switch(priority) {
        case LOW_PRIORITY:
            painter->setBrush(QBrush(Qt::green));
            break;
        case MEDIUM_PRIORITY:
            painter->setBrush(QBrush(Qt::yellow));
            break;
        case HIGH_PRIORITY:
            painter->setBrush(QBrush(Qt::red));
            break;
        }

        painter->drawEllipse(rect);
    } else {
        //Object
        QPen pen2(Qt::black, 0);
        painter->setPen(pen2);

        path->setChoosen(false);
        predictedPath->setChoosen(false);

        switch(priority) {
        case LOW_PRIORITY:
            painter->setBrush(QBrush(Qt::green));
            break;
        case MEDIUM_PRIORITY:
            painter->setBrush(QBrush(Qt::yellow));
            break;
        case HIGH_PRIORITY:
            painter->setBrush(QBrush(Qt::red));
            break;
        }

        painter->drawEllipse(rect);
    }
}

void Obj::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    if(event->button() == Qt::LeftButton) {
        clicked = true;
        chosen = true;
        update();
    }
    QGraphicsItem::mousePressEvent(event);
}

QComboBox *Obj::getPriorityBox() const {
    return priorityBox;
}

void Obj::setPriorityBox(QComboBox *value) {
    priorityBox = value;
}

QTableWidgetItem *Obj::getNameLabel() const {
    return nameLabel;
}

void Obj::setNameLabel(QTableWidgetItem *value) {
    nameLabel = value;
}

QTableWidgetItem *Obj::getTypeLabel() const {
    return typeLabel;
}

void Obj::setTypeLabel(QTableWidgetItem *value) {
    typeLabel = value;
}

void Obj::updatePos(int time) {
    if(time - timeCreated > 0) {
        show();
        if(showRealPath) {
            path->show();
            predictedPath->show();
        } else {
            path->hide();
            predictedPath->hide();
        }
        spottedPath->show();
        if(spottedPath->getSize() != 0) {
            setPos(spottedPath->getCurrentPos()->x, -spottedPath->getCurrentPos()->y);
        }
        spottedPath->setTime(time - timeCreated);
        path->setTime(time - timeCreated);

        if(!predicted) {predicted = predict();}
        predictedPath->update();
    } else {
        if(time - timeCreated < 0) {
            hide();
        }
        if(!showRealPath) {
            path->hide();
            predictedPath->hide();
            spottedPath->hide();
        }
    }
}

bool Obj::spotted(double chance) {
    if(chance <= spottedChance) {
        spottedPath->addPos(path->getLastPos());
        return true;
    }
    return false;
}

void Obj::analyze() {
    double PI = 3.1415926535;
    if(!analyzed && spottedPath->getSize() >= 20) {
        QVector<double> courses;
        for(int i = 0; i < spottedPath->getSize(); i++) {
            for(int j = 0; j < courses.size();j++) {
                if(courses.at(j) == spottedPath->getPos(i)->course) {
                    break;
                } else {
                    if(j == courses.size() - 1) {
                        courses.append(spottedPath->getPos(i)->course);
                    }
                }
            }
        }

        if(courses.size() == 3) {
            double genCourse = 361;
            double width = -1;
            double length = 9999;

            bool hasCounter = false;
            for(int i = 0; i < courses.size(); i++) {
                for(int j = 0; j < courses.size(); j++) {
                    if(abs(courses.at(i) - courses.at(j)) == 180) {
                        hasCounter = true;
                        break;
                    }
                    genCourse = courses.at(i);
                }
            }

            if(hasCounter) {
                double angle = PI * (450 - genCourse) / 180;
                for(int i = 0; i < spottedPath->getSize(); i++) {
                    for(int j = i + 1; j < spottedPath->getSize(); j++) {
                        double displaycedX = spottedPath->getPos(i)->x - spottedPath->getPos(j)->x;
                        double displaycedY = spottedPath->getPos(i)->y - spottedPath->getPos(j)->y;
                        double transformedX = displaycedX * cos(angle) - displaycedY * sin(angle);
                        double transformedY = displaycedX * sin(angle) + displaycedY * cos(angle);
                        if (abs(transformedX) > width) {
                            width = abs(transformedX);
                        }

                        if (abs(spottedPath->getPos(i)->course - spottedPath->getPos(j)->course) == 180) {
                            if (abs(transformedY) < length) {
                                length = abs(transformedY);
                            }
                        }
                    }
                }
                if ((width != -1) && (length != 9999)) {

                    analyzed = true;
                }
            }
        }

        if(!analyzed) {
            double genCourse = spottedPath->getMeanCourse();
            double width = -1;
            double deltaCourse = -1;

            double diff1 = 0, diff2 = 9999;
            double angle = PI * (450 - genCourse) / 180;
            for(int i = 0; i < spottedPath->getSize(); i++) {
                for(int j = i + 1; j < spottedPath->getSize(); j++) {
                    double displaycedX = spottedPath->getPos(i)->x - spottedPath->getPos(j)->x;
                    double displaycedY = spottedPath->getPos(i)->y - spottedPath->getPos(j)->y;
                    double transformedX = displaycedX * cos(angle) - displaycedY * sin(angle);

                    if(abs(transformedX) > width) {
                        width = abs(transformedX);
                    }

                    if(abs(spottedPath->getPos(i)->course - spottedPath->getPos(j)->course) > diff1) {
                        diff1 = abs(spottedPath->getPos(i)->course - spottedPath->getPos(j)->course);
                    }

                    if(abs(spottedPath->getPos(i)->course - spottedPath->getPos(j)->course) < diff2) {
                        diff2 = abs(spottedPath->getPos(i)->course - spottedPath->getPos(j)->course);
                    }
                }
            }

            deltaCourse = diff1 + diff2;
            if ((width > 0) && (deltaCourse < 150) ) {

                analyzed = true;
            }
        }

        if(!analyzed) {
            double r = 0;
            for(int i = 0; i < spottedPath->getSize(); i++) {
                for(int j = i + 1; j < spottedPath->getSize(); j++) {
                    double diffX = spottedPath->getPos(i)->x - spottedPath->getPos(j)->x;
                    double diffY = spottedPath->getPos(i)->y - spottedPath->getPos(j)->y;
                    double length = sqrt(diffX * diffX + diffY * diffY);

                    if(length/2 > r) {
                        r = length/2;
                    }

                    int half = spottedPath->getSize() / 2;
                    double firstMeanX = 0, secondMeanX = 0;
                    double firstMeanY = 0, secondMeanY = 0;

                    for(int i = 0; i < half; i++) {
                        firstMeanX += spottedPath->getPos(i)->x;
                        secondMeanX += spottedPath->getPos(i + half)->x;

                        firstMeanY += spottedPath->getPos(i)->y;
                        secondMeanY += spottedPath->getPos(i + half)->y;
                    }

                    firstMeanX /= half;
                    secondMeanX /= half;

                    firstMeanY /= half;
                    secondMeanY /= half;

                    if(firstMeanX > secondMeanX) {
                        diffX = (firstMeanX - secondMeanX) / firstMeanX;
                    } else {
                        diffX = (secondMeanX - firstMeanX) / secondMeanX;
                    }

                    if(firstMeanY > secondMeanY) {
                        diffY = (firstMeanY - secondMeanY) / firstMeanY;
                    } else {
                        diffY = (secondMeanY - firstMeanY) / secondMeanY;
                    }

                    if(diffX <= 0.1 && diffY <= 0.1) {

                        analyzed = true;
                    }
                }
            }
        }

        if(!analyzed) {

            analyzed = true;
        }
    }
}

bool Obj::predict() {
    if(path->getType() == 0) {
        predictedPath = new Breach(path->getPos(0), 135, 50, 24);
        predictedPath->setSimulated(true);
        predictedPath->setParentItem(path->parentItem());

        while((predictedPath->getLastPos()->x < 650 && predictedPath->getLastPos()->x > 0 && predictedPath->getLastPos()->y > -700 && predictedPath->getLastPos()->y < 0)) {
            predictedPath->update();
        }
        predictedPath->setDrawBounds(true);
        return true;
    } else if(path->getType() == 1) {
        predictedPath = new Scouting(path->getPos(0),135, 40, 20);
        predictedPath->setSimulated(true);
        predictedPath->setParentItem(path->parentItem());

        while((predictedPath->getLastPos()->x < 650 && predictedPath->getLastPos()->x > 0 && predictedPath->getLastPos()->y > -700 && predictedPath->getLastPos()->y < 0)) {
            predictedPath->update();
        }
        predictedPath->setDrawBounds(true);
        return true;
    }
}

QString Obj::getPriorityName() {
    switch(priority) {
    case 4:
        return "������";
    case 5:
        return "�������";
    case 6:
        return "�������";
    default:
        return "����������";
    }
}

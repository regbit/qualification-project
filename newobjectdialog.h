#ifndef NEWOBJECTDIALOG_H
#define NEWOBJECTDIALOG_H

#include <QDialog>
#include "obj.h"

namespace Ui {
class NewObjectDialog;
}

class NewObjectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewObjectDialog(QWidget *parent = 0);
    ~NewObjectDialog();
    Obj* getObject() {return obj;}

private slots:
    void on_buttonBox_accepted();

    void on_comboBox_currentIndexChanged(int index);

private:
    Ui::NewObjectDialog *ui;
    Obj *obj;
};

#endif // NEWOBJECTDIALOG_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include "obj.h"
#include "layer.h"
#include "newobjectdialog.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    static const int LOW = 0, MEDIUM = 1, HIGH = 2;
    static int timePassed, currentTime, quaters;
    static int timePassedSub, currentTimeSub, quatersSub;
    int id;

    void obejctSpotted(Pos* pos);

    explicit MainWindow(QWidget *parent = 0);
    void closeEvent(QCloseEvent *event);
    ~MainWindow();

protected:
    virtual void wheelEvent(QWheelEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void timerEvent(QTimerEvent *event);

private slots:
    void on_playPause_clicked();

    void on_tableWidget_pressed(const QModelIndex &index);

    void on_stepForward_clicked();

    void on_stepBackward_clicked();

    void on_timeline_sliderMoved(int position);

    void on_action_7_triggered();

    void on_action_5_triggered();

private:
    static int nextId;
    void newObject(Path *path, QString name, int timeCreated);
    void newObject(Obj *obj);
    void onEvent();
    bool pause;
    int zoom, timer, threatLevel;
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    QList<Obj*> objects;
    Obj *chosen;
    Layer *bg, *fg;
    NewObjectDialog *nod;
    MainWindow *sub;
};

#endif // MAINWINDOW_H

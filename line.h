#ifndef LINE_H
#define LINE_H

#include <QtCore>
#include <QtGui>
#include "pos.h"

class Line : public QGraphicsItem {
public:
    Line();
    Line(Pos* beginning);
    Line(Pos* beginning, Pos* end);

    double dx, dy;
    int forceLast;

    QPen pen;

    void update();

    void addPos(Pos* newPos);
    void setSimulated(bool isSimulated) {simulated = isSimulated;}
    Pos* get(int i) const;
    Pos* getBeginning() const;
    Pos* getEnd() const;
    int getSize();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);

private:

    QVector<Pos*> coordinates;
    bool simulated;

};

#endif // LINE_H

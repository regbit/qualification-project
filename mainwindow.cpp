#include "mainwindow.h"
#include "obj.h"
#include "pos.h"
#include "path.h"
#include "breach.h"
#include "scouting.h"
#include "searching.h"
#include "ui_mainwindow.h"
#include "newobjectdialog.h"
#include <QtCore/QTextCodec>

int MainWindow::timePassed = 0;
int MainWindow::currentTime = 0;
int MainWindow::quaters = 0;

int MainWindow::timePassedSub = 0;
int MainWindow::currentTimeSub = 0;
int MainWindow::quatersSub = 0;

int MainWindow::nextId = 0;

///
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);

    id = nextId;
    nextId = 1;

    if(id == 0) {
        ui->action_7->setVisible(false);
        if(nextId == 2) {
            ui->action_5->setVisible(false);
        }
    }
    if(id == 1) {
        setWindowTitle("Imitation");
        ui->action_5->setVisible(false);
    }

    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);

    bg = new Layer();
    fg = new Layer();

    scene->addItem(bg);
    scene->addItem(fg);

    pause = false;
    zoom = 1;
    scene->setSceneRect(0, 0, 700, 650);

    if(id == 1) {
        newObject(new Breach(new Pos(50, -200, 160, 4.0), 135, 50, 24), "1", 0);
        objects.last()->setSimulated(true);
        /*newObject(new Scouting(new Pos(100, -100, 225, 4.0, true), 135, 40, 20), "2", 0);
        objects.last()->setSimulated(true);*/
        /*newObject(new Searching(new Pos(300, -200, 100, 4.0), 50), "3", 0);
        objects.last()->getPath()->setSimulated(true);*/
    }

    chosen = NULL;
    nod = NULL;

    timer = startTimer(100);
}

void MainWindow::mousePressEvent(QMouseEvent *event) {
    if(event->button() == Qt::LeftButton) {
        int a = 0;
        chosen = NULL;
        foreach (Obj *obj, objects) {
            if(!obj->isClicked()) {
                obj->setChosen(false);
            } else {
                chosen = obj;
                obj->setClicked(false);
            }
            obj->update();
            a++;
        }
    }
    onEvent();
}

void MainWindow::timerEvent(QTimerEvent *event) {
    //Objects update
    if(id == 0) {
        currentTime = timePassed + ui->timeline->value();
        quaters++;
        if(quaters > 0 && quaters % 4 == 0) {
            foreach(Obj *obj, objects) {
                obj->getPath()->update();
            }
            timePassed++;
        }
    } else {
        currentTimeSub = timePassedSub + ui->timeline->value();
        quatersSub++;
        if(quatersSub > 0 && quatersSub % 4 == 0) {
            foreach(Obj *obj, objects) {
                obj->getPath()->update();
            }
            timePassedSub++;
        }
    }

    scene->update(-50,-50,750,700);
    onEvent();
}

void MainWindow::newObject(Path *path, QString name, int timeCreated) {
        newObject(new Obj(path, name, timeCreated));
    }

void MainWindow::newObject(Obj *obj) {
        Path *path = obj->getPath();
        QString name = obj->getName();
        objects.append(obj);

        obj->getPredictedPath()->setParentItem(bg);
        obj->getPath()->setParentItem(bg);
        obj->getSpottedPath()->setParentItem(bg);
        obj->setParentItem(fg);

        ui->tableWidget->setRowCount(ui->tableWidget->rowCount() + 1);

        //Name
        QTableWidgetItem* item = new QTableWidgetItem;
        item->setText(tr(name.toAscii()));
        item->setTextAlignment(Qt::AlignCenter);

        Qt::ItemFlags eFlags = item->flags();
        eFlags &= ~Qt::ItemIsEditable;
        item->setFlags(eFlags);

        ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 0, item);
        obj->setNameLabel(item);

        //Type
        item = new QTableWidgetItem;
        item->setText(tr(path->getName().toAscii()));
        item->setTextAlignment(Qt::AlignCenter);
        item->setFlags(eFlags);

        ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 1, item);
        obj->setTypeLabel(item);

        //Priority
        QComboBox *comboBox = new QComboBox(this);
        comboBox->addItem(tr("�����"), QVariant(4));
        comboBox->addItem(tr("�������"), QVariant(5));
        comboBox->addItem(tr("�������"), QVariant(6));
        ui->tableWidget->setCellWidget(ui->tableWidget->rowCount() - 1, 2, comboBox);
        obj->setPriorityBox(comboBox);
    }


void MainWindow::onEvent() {
    if(id == 1) {

        if(nod != NULL) {
            if(nod->getObject() != NULL) {
                newObject(nod->getObject());
                objects.last()->setTimeCreated(timePassedSub);
                nod = NULL;
            }
        }
    }

    threatLevel = ui->threatLevelBox->currentIndex();

    if(id == 0) {
        if(quaters > 0 && quaters % 4 == 0) {
            ui->timeline->setMinimum(-timePassed);
            if(pause) {
                ui->timeline->setValue(ui->timeline->value() - 1);
            }
        }

        foreach(Obj *obj, objects) {
            obj->updatePos(timePassed + ui->timeline->value() + 1);
            obj->setPriority(obj->getPriorityBox()->itemData(obj->getPriorityBox()->currentIndex()).toInt());
            obj->spotted((double)(rand() % 100) / 100.0);
            obj->update();
        }

        ui->time->setText(QString("-") + QString::number(abs(ui->timeline->value()/60)) + QString(":") + QString::number(abs(ui->timeline->value()%60)) +
                          QString("/") + QString::number(abs(timePassed/60)) + QString(":") + QString::number(abs(timePassed%60)));
    } else {
        if(quatersSub > 0 && quatersSub % 4 == 0) {
            ui->timeline->setMinimum(-timePassedSub);
            if(pause) {
                ui->timeline->setValue(ui->timeline->value() - 1);
            }
        }

        foreach(Obj *obj, objects) {
            obj->updatePos(timePassedSub + ui->timeline->value() + 1);
            obj->setPriority(obj->getPriorityBox()->itemData(obj->getPriorityBox()->currentIndex()).toInt());
            obj->spotted((double)(rand() % 100) / 100.0);
            obj->update();
        }

        ui->time->setText(QString("-") + QString::number(abs(ui->timeline->value()/60)) + QString(":") + QString::number(abs(ui->timeline->value()%60)) +
                          QString("/") + QString::number(abs(timePassedSub/60)) + QString(":") + QString::number(abs(timePassedSub%60)));
    }

    if(chosen == NULL) {
        ui->name->setText(QString(""));
        ui->type->setText(QString(""));
        ui->coord->setText(QString("0, 0"));
        ui->v->setText(QString("0"));
        ui->course->setText(QString("0"));
        ui->RMSCoord->setText(QString("0, 0"));
        ui->RMSV->setText(QString("0"));
        ui->RMSCourse->setText(QString("0"));

        ui->tableWidget->clearSelection();
    } else {
        ui->name->setText(tr(QString(chosen->getName()).toAscii()));
        ui->type->setText(tr(QString(chosen->getPath()->getName()).toAscii()));
        ui->coord->setText(QString::number(chosen->getX()) + QString(", ") + QString::number(chosen->getY()));
        ui->v->setText(QString::number(chosen->getV()));
        ui->course->setText(QString::number(chosen->getCourse()));
        ui->RMSCoord->setText(QString::number(chosen->getRootMeanSquareX()) + QString(", ") + QString::number(chosen->getRootMeanSquareY()));
        ui->RMSV->setText(QString::number(chosen->getRootMeanSquareV()));
        ui->RMSCourse->setText(QString::number(chosen->getRootMeanSquareCourse()));

        ui->tableWidget->selectRow(chosen->getNameLabel()->row());
    }
}

void MainWindow::wheelEvent(QWheelEvent *event) {
    /*ui->graphicsView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    double scaleFactor = 1.15;
    if(event->delta() > 0 & zoom < 8) {
        ui->graphicsView-> scale(scaleFactor, scaleFactor);
        zoom++;
    } else if(zoom > 1) {
        ui->graphicsView->scale(1.0 / scaleFactor, 1.0 / scaleFactor);
        zoom--;
    }*/
}

MainWindow::~MainWindow() {
    killTimer(timer);
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event) {
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, tr("����������\n"), tr("������� ����?\n"), QMessageBox::No | QMessageBox::Yes, QMessageBox::Yes);
       if (resBtn != QMessageBox::Yes) {
           event->ignore();
       } else {
           event->accept();
           killTimer(timer);
           foreach (Obj *o, objects) {
               delete o;
           }
           timePassedSub = 0;
           currentTimeSub = 0;
           quatersSub = 0;
       }
}

void MainWindow::on_playPause_clicked() {
    pause = !pause;
}

void MainWindow::on_tableWidget_pressed(const QModelIndex &index) {
    int a = 0;
    chosen = NULL;
    foreach (Obj *obj, objects) {
        if(a != index.row()) {
            obj->setChosen(false);
        } else {
            chosen = obj;
            obj->setChosen(true);
        }
        obj->update();
        a++;
    }
}


void MainWindow::on_stepForward_clicked() {
    ui->timeline->setValue(ui->timeline->value()+1);
}

void MainWindow::on_stepBackward_clicked() {
    ui->timeline->setValue(ui->timeline->value()-1);
}

void MainWindow::on_timeline_sliderMoved(int position) {
    onEvent();
}

void MainWindow::on_action_7_triggered() {
    nod = new NewObjectDialog(this);
    nod->show();
}

void MainWindow::on_action_5_triggered() {
    sub = new MainWindow(this);
    timePassedSub = 0;
    currentTimeSub = 0;
    quatersSub = 0;
    sub->show();
}

#ifndef SCOUTING_H
#define SCOUTING_H
#include "path.h"

class Scouting : public Path {

public:
    Scouting();
    Scouting(Pos* initPos, double genCourse, double width, double length);
    Scouting(Path* path);
    QString getName(){return "��������";}

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void update();
private:
    double genCourse, width, length;
};

#endif // SCOUTING_H

#include "newobjectdialog.h"
#include "ui_newobjectdialog.h"
#include "path.h"
#include "breach.h"
#include "scouting.h"
#include "searching.h"
#include "mainwindow.h"

NewObjectDialog::NewObjectDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewObjectDialog) {
    obj = NULL;
    ui->setupUi(this);
    ui->label_chance->setText(tr(QString(" ").toAscii()));
    ui->lineChance->hide();
    ui->label_A->setText(tr(QString(" ").toAscii()));
    ui->lineA->hide();
    ui->label_B->setText(tr(QString(" ").toAscii()));
    ui->lineB->hide();
    ui->label_C->setText(tr(QString(" ").toAscii()));
    ui->lineC->hide();
}

NewObjectDialog::~NewObjectDialog() {
    delete ui;
}

void NewObjectDialog::on_buttonBox_accepted() {
    if(ui->lineX->text().length() > 0 &&
            ui->lineY->text().length() > 0 &&
            ui->lineCourse->text().length() > 0 &&
            ui->lineVel->text().length() > 0 &&
            ui->lineA->text().length() > 0 &&
            ui->lineB->text().length() > 0 &&
            ui->lineC->text().length() > 0 && ui->lineChance->text().length() > 0) {
        double paramA = ui->lineA->text().toDouble();
        double paramB = 0;
        if(ui->lineB->text().length() != 0) {
            paramB = ui->lineB->text().toDouble();
        }
        double paramC = 0;
        if(ui->lineC->text().length() != 0) {
            paramC = ui->lineC->text().toDouble();
        }

        if(ui->comboBox->currentIndex() == 1) {
            obj = new Obj(new Breach(new Pos(ui->lineX->text().toDouble(), -ui->lineY->text().toDouble(), ui->lineCourse->text().toDouble(), ui->lineVel->text().toDouble(), true), paramA, paramB, paramC), tr(QString(ui->lineName->text()).toAscii()), 0);
            obj->setSpottedChance((double)ui->lineChance->text().toInt() / 100.0);
            obj->setSimulated(true);
        } else  if(ui->comboBox->currentIndex() == 2){
            obj = new Obj(new Scouting(new Pos(ui->lineX->text().toDouble(), -ui->lineY->text().toDouble(), ui->lineCourse->text().toDouble(), ui->lineVel->text().toDouble(), true), paramA, paramB, paramC), tr(QString(ui->lineName->text()).toAscii()), 0);
            obj->setSpottedChance((double)ui->lineChance->text().toInt() / 100.0);
            obj->setSimulated(true);
        } else  if(ui->comboBox->currentIndex() == 3){
            obj = new Obj(new Searching(new Pos(ui->lineX->text().toDouble(), -ui->lineY->text().toDouble(), ui->lineCourse->text().toDouble(), ui->lineVel->text().toDouble(), true), paramA), tr(QString(ui->lineName->text()).toAscii()), 0);
            obj->setSpottedChance((double)ui->lineChance->text().toInt() / 100.0);
            obj->setSimulated(true);
        }
    }
}

void NewObjectDialog::on_comboBox_currentIndexChanged(int index) {
    if(index == 0) {
        ui->label_chance->setText(tr(QString(" ").toAscii()));
        ui->lineChance->hide();
        ui->label_A->setText(tr(QString(" ").toAscii()));
        ui->lineA->hide();
        ui->label_B->setText(tr(QString(" ").toAscii()));
        ui->lineB->hide();
        ui->label_C->setText(tr(QString(" ").toAscii()));
        ui->lineC->hide();
    }
    else if(index == 1) {
        ui->label_chance->setText(tr(QString("���� �����������").toAscii()));
        ui->lineChance->show();
        ui->label_A->setText(tr(QString("����������� ����").toAscii()));
        ui->lineA->show();
        ui->label_B->setText(tr(QString("��������� �����").toAscii()));
        ui->lineB->show();
        ui->label_C->setText(tr(QString("����������").toAscii()));
        ui->lineC->show();
    } else if(index == 2) {
        ui->label_chance->setText(tr(QString("���� �����������").toAscii()));
        ui->lineChance->show();
        ui->label_A->setText(tr(QString("����������� ����").toAscii()));
        ui->lineA->show();
        ui->label_B->setText(tr(QString("������").toAscii()));
        ui->lineB->show();
        ui->label_C->setText(tr(QString("������").toAscii()));
        ui->lineC->show();
    } else if(index == 3) {
        ui->label_chance->setText(tr(QString("���� �����������").toAscii()));
        ui->lineChance->show();
        ui->label_A->setText(tr(QString("������").toAscii()));
        ui->lineA->show();
        ui->label_B->setText(tr(QString(" ").toAscii()));
        ui->lineB->hide();
        ui->label_C->setText(tr(QString(" ").toAscii()));
        ui->lineC->hide();
    }
}

#ifndef PATH_H
#define PATH_H

#include <QtCore>
#include <QtGui>
#include "pos.h"
#include "line.h"

class Path : public QGraphicsItem {

public:

    static const int UNDEFINED = 0, BREACH = 1, SCOUTING = 2, SEARCHING = 3, UNKNOWN = 4;
    static const double PI = 3.1415926535;

    Path();
    Path(Pos* initPos);

    QPen pen;

    Pos* getPos(int index);
    Pos* getCurrentPos();
    Pos* getLastPos();
    Pos* getTurningPoint(int i) {if(i<turningPoints.size()) return turningPoints.at(i);}
    virtual QString getName(){}
    int getSize() {return coordHistory.size();}
    int getType() {return type;}
    int getTurningPointsCount() {return turningPoints.size();}
    virtual void update(){}
    void addPos(Pos* newPos);
    void setTime(int time) {this->time = time;}
    void clear() {coordHistory.clear();}

    void setChoosen(bool choosen);
    void setSimulated(bool isSimulated){simulated = isSimulated; foreach (Line *l, lines) {l->setSimulated(isSimulated);}}
    void setDrawBounds(bool drawBounds){this->drawBounds = drawBounds;}

    bool isChoosen(){return choosen;}
    bool isSimulated(){return simulated;}

    int getTime(){return time;}
    int getLastTurningPointIndex(){return lastTurningPointIndex;}

    QVector<Pos*> getCoordHistory(){return coordHistory;}
    QVector<Pos*> getTurningPoints(){return turningPoints;}
    QVector<Line*> getLines(){return lines;}

    double getMeanCourse();
    double getRootMeanSquareX();
    double getRootMeanSquareY();
    double getRootMeanSquareCourse();
    double getRootMeanSquareV();
    QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

protected:
    QVector<Pos*> coordHistory;
    QVector<Pos*> turningPoints;
    QVector<Line*> lines;
    int time, lastTurningPointIndex, type;
    bool choosen, simulated, drawBounds;

};

#endif // PATH_H

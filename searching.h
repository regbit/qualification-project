#ifndef SEARCHING_H
#define SEARCHING_H
#include "path.h"

class Searching : public Path {

public:
    Searching();
    Searching(Pos* initPos, double radius);
    Searching(Path* path);
    QString getName(){return "�����";}

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void update();
private:
    double radius;
};

#endif // SEARCHING_H


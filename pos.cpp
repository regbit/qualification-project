#include "pos.h"
#include "mainwindow.h"

Pos::Pos(bool isSimulated) {
    x = 0;
    y = 0;
    course = 0;
    v = 0;
    simulated = isSimulated;
    if(!isSimulated) {
        time = MainWindow::timePassed;
    } else {
        time = MainWindow::timePassedSub;
    }
}

Pos::Pos(double x, double y, double course, double v, bool isSimulated) {
    this->x = x;
    this->y = y;
    this->course = course;
    this->v = v;
    simulated = isSimulated;
    if(!isSimulated) {
        time = MainWindow::timePassed;
    } else {
        time = MainWindow::timePassedSub;
    }
}
